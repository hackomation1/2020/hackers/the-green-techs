Kind: Statement of Work
Title: <hello>
Author: Hilton Lipschitz
Affiliation: <Company Name>
Date: 2016-03-04
Version: 0.1

# Project Title: [hello]

## Overview

<What is the objective/goal is of this work>

## Tasks

<A list of the tasks to be performed — very high level, clearly written>

## Deliverables

<The deliverables of the project, so we know when it is complete>
